
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

/**Clase para contruir un cliente nuevo  y  

 * traer datos de la base de datos asignandoselos a los this. de esta clase
 * 
 * @author Amariliz
 * */
public class Cliente {
	private String nombre;
	private String apellido;
	private String dni;

	private String correo;
	private int tarjeta;

	// contructor

	public Cliente(String nombre, String apellido, String dni, String correo, int tarjeta) {
		this.setNombre(nombre);
		this.setApellido(apellido);
		this.setDni(dni);
		this.setCorreo(correo);
		this.setTarjeta(tarjeta);
	}

	/**
	 *  se setean los datos del Cliente 
	 * @param tarjeta numero de tarjeta de cliente */
	public Cliente(int tarjeta) {
		this.setTarjeta(tarjeta);
	}

	/**
	 * Este metodo busca en la Base de datos utilizando el numero de tarjeta
	 * 
	 * Coge una conexion  y hace una consulta a la base de datos 
	 *  si los datos no existen lanza una exeption
	 * que se manejara en el manejador de errores 
	 * @throws ClienteNoEncontradoException esta excepcion se lanzara si no se encuentra el cliente en la consulta de la base de datos
	 * 
	 */
	public void consultarBD() throws ClienteNoEncontradoException {
		try {
			Statement s = Conexion.conexion.createStatement();
			ResultSet rs = s.executeQuery("SELECT * FROM Cliente WHERE codigoTarjeta =" + this.tarjeta);

			if (!rs.next()) {
				throw new ClienteNoEncontradoException();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Este metodo inserta un cliente en la base de datos 
	 * 
	 */
	public void insertar() {
		try {
			Statement s = Conexion.conexion.createStatement();
			int rsb = s.executeUpdate(
					"INSERT INTO  Cliente (Nombre,Apellido,Dni,Correo,codigoTarjeta) VALUES ('" + this.nombre + "', '"
							+ this.apellido + "', '" + this.dni + "', '" + this.correo + "', " + this.tarjeta + ")");
			JOptionPane.showMessageDialog(null, "Guardado.", "alert", JOptionPane.ERROR_MESSAGE);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// setters and getters
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getDni() {
		return dni;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public int getTarjeta() {
		return tarjeta;
	}

	public void setTarjeta(int tarjeta) {
		this.tarjeta = tarjeta;
	}

	@Override
	public String toString() {
		return "Cliente [nombre=" + nombre + ", apellido=" + apellido + ", dni=" + dni + ", correo=" + correo
				+ ", tarjeta=" + tarjeta + "]";
	}

}
