

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.rowset.spi.TransactionalWriter;
import javax.swing.JOptionPane;
/**
 *  Esta Clase se usara para las conexiones a la base de datos 
 *  */

abstract public class Conexion {	
	public static Connection conexion;
	static Statement s;
	static ResultSet rs;
	
	
	/**
	 * Este metodo crea la conexion con la base de datos local
	 * */
	public static void conectar (){
		try{
			conexion = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/db_reservas?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC",
						"root", "admin0011");
			 System.out.println("Exito");
			 
			}catch (Exception ex) {
				System.out.println("Fallo");
			}
	}
	
	
	
	/**
	 *  Este metodo hace conexion y 
	 * creara la tabla Cliente que usaremos para insertar los datos 
	 * crea la tabla con los parametros necesarios 
	 * e inserta un cliente de prueba 
	 * */
	
	public static void iniciarBaseDeDatos(){
		try{
        Statement smt = conexion.createStatement();
     
        smt.executeUpdate("CREATE TABLE IF NOT EXISTS Cliente ( Nombre varchar(12), Apellido varchar(12),Dni varchar(9),Correo varchar(30) ,codigoTarjeta int(10)primary key ); ");

		/*envia dato*/
        Statement smt2=conexion.createStatement();
		smt2.executeUpdate("INSERT INTO  Cliente (Nombre,Apellido,Dni,Correo,codigoTarjeta) VALUES ('Amariliz2', 'Oyarzun2', 'y22817072', 'correofalso2@gmail.com', 123456782)");	

		}catch(Exception ex){
			System.out.println(ex.getMessage());
		}
	

	}
}
