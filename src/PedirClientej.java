import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import javax.swing.Action;
import javax.swing.JTextField;
import java.awt.Font;

public class PedirClientej extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private final Action action = new SwingAction();
	private JTextField jsNumeroTarjetaPedirCliente;
	private final Action action_1 = new SwingAction_1();

	/**
	 * Launch the application.
	 * @param args pedir cliente
	 */
	public static void main(String[] args) {
		try {
			PedirClientej dialog = new PedirClientej();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public PedirClientej() {
		
		setModalityType(ModalityType.APPLICATION_MODAL);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_contentPanel.rowHeights = new int[]{0, 0, 0, 0};
		gbl_contentPanel.columnWeights = new double[]{0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_contentPanel.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPanel.setLayout(gbl_contentPanel);
		{
			JLabel lblNumeroTarjeta = new JLabel("Numero tarjeta");
			lblNumeroTarjeta.setFont(new Font("Lucida Grande", Font.PLAIN, 20));
			GridBagConstraints gbc_lblNumeroTarjeta = new GridBagConstraints();
			gbc_lblNumeroTarjeta.insets = new Insets(0, 0, 5, 5);
			gbc_lblNumeroTarjeta.gridx = 1;
			gbc_lblNumeroTarjeta.gridy = 1;
			contentPanel.add(lblNumeroTarjeta, gbc_lblNumeroTarjeta);
		}
		{
			jsNumeroTarjetaPedirCliente = new JTextField();
			GridBagConstraints gbc_jsNumeroTarjetaPedirCliente = new GridBagConstraints();
			gbc_jsNumeroTarjetaPedirCliente.gridwidth = 7;
			gbc_jsNumeroTarjetaPedirCliente.insets = new Insets(0, 0, 5, 5);
			gbc_jsNumeroTarjetaPedirCliente.fill = GridBagConstraints.HORIZONTAL;
			gbc_jsNumeroTarjetaPedirCliente.gridx = 3;
			gbc_jsNumeroTarjetaPedirCliente.gridy = 1;
			contentPanel.add(jsNumeroTarjetaPedirCliente, gbc_jsNumeroTarjetaPedirCliente);
			jsNumeroTarjetaPedirCliente.setColumns(10);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Aceptar");
				okButton.setToolTipText("");
				okButton.setAction(action);
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setAction(action_1);
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
				
			}
		}
		setVisible(true);
	}

	
	private class SwingAction extends AbstractAction {
		public SwingAction() {
			putValue(NAME, "Aceptar");
			
			
		}
		/*Guarda los datos de cliente  al pulsar el boton aceptar , llamando al formularioCliente y guardando sus valores*/
		public void actionPerformed(ActionEvent e) {
			System.out.println("se ha cliqueado");
			FormularioCliente.numeroTarjeta = Integer.parseInt(jsNumeroTarjetaPedirCliente.getText());
			dispose();
		}
		
		
	}
	private class SwingAction_1 extends AbstractAction {
		public SwingAction_1() {
			putValue(NAME, "Cancelar");
		
		}
		public void actionPerformed(ActionEvent e) {
			dispose();
		}
	}
}
