

import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;

/**
 * Clase para crear Vuelos  utilizando Ciudad, Avion y la clase Random
 * */
public class Vuelo {
//atributos 
	private int fechaSalida;
	private int  fechaLlegada;
	private Ciudad origen;
	private Ciudad destino;
	private Avion avion;
	private int codigo;
	private Random random;

//contructor 
	Vuelo(int fechaSalida, int fechaLlegada,Ciudad o,Ciudad d, Avion avion) {
		this.fechaSalida = fechaSalida;
		this.fechaLlegada = fechaLlegada;
		this.origen=o;
		this.destino=d;
		this.avion = avion;
		this.random=new Random();
		this.codigo=this.random.nextInt(10000);	
	}
	
	
	// setters and getters
	public int  getFechaSalida() {
		return fechaSalida;
	}
	public void setFechaSalida(int  fechaSalida) {
		this.fechaSalida = fechaSalida;
	}
	public int  getFechaLlegada() {
		return fechaLlegada;
	}
	public void setFechaLlegada(int  fechaLlegada) {
		this.fechaLlegada = fechaLlegada;
	}
	public Avion getAvion() {
		return avion;
	}
	public void setAvion(Avion avion) {
		this.avion = avion;
	}
	public Ciudad getOrigen() {
		return origen;
	}
	public void setOrigen(Ciudad origen) {
		this.origen = origen;
	}
	public Ciudad getDestino() {
		return destino;
	}
	public void setDestino(Ciudad destino) {
		this.destino = destino;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}


	@Override
	public String toString() {
		return "\n--------Vuelo---------\n\n Fecha Salida= " + fechaSalida +" Julio"+ " \n fecha Llegada= " + fechaLlegada +" Julio"+ "\n Origen= " + origen
				+ "\n Destino= " + destino + "\n"+ avion+"\n codigo avion "+codigo ;
	}	
	
}
