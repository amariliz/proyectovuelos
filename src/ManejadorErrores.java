import java.util.InputMismatchException;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
/**
 * Esta clase maneja los errores posibles  de vuelo (vuelo no esta en el array) , y cliente (si cliente no esta en la bs)
 * 
 * */
public class ManejadorErrores {
	/**
	 * Este metodo resuelve el problema dado si es posible 
	 * Polimorfismo de excepciones InputMismatchException(de java) VueloNoEncontradoException y ClienteNoEncontradoException 
	 * 
	 * llama a PedirCliente si el cliente no es encontrado
	 * @param error la exception contemplada 
	 * */
	public void manejar(Exception error) {
		if (error instanceof InputMismatchException) {
			System.out.println("formato incorrecto");
		} else if (error instanceof VueloNoEncontradoException) {
			System.out.println(error.getMessage());
		} else if (error instanceof ClienteNoEncontradoException) {
			 JOptionPane.showMessageDialog(null, "No se ha encontrado , registrese", "alert", JOptionPane.ERROR_MESSAGE);
			 new PedirClienteNuevoj();
		}
	}
	
}
