
import java.util.ArrayList;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Iterator;
import java.util.Scanner;
/**
 *  Clase abstracta solo para almacenar dististas funcionalidades de mostrar datos
 *  */

abstract public class Utilidades {

	
	/*Este metodo almacena en un mapa los asientos que estan disponibles */
	public static void notificarAsientosDisponibles(HashMap<String, Asiento> asientosLibres) {
		String mensaje = "asientos libres " + asientosLibres.size();

		for (Asiento asiento : asientosLibres.values()) {

			mensaje += "asientos " + asiento.getCoordenada();
		}
		System.out.println(mensaje);
	}

	/**
	 * Muestra vuelos disponibles desde un array
	 * @param vuelos es un arraylist de vuelos 
	 */
	public static void mostrarVuelosDisponibles(ArrayList<Vuelo> vuelos) {

		Iterator<Vuelo> vuelosdisponibles = vuelos.iterator();
		while (vuelosdisponibles.hasNext()) {
			Vuelo elemento = vuelosdisponibles.next();
			System.out.print(elemento + " / ");
		}
	}

	/**
	 *  Este metodo es para pedir una fecha de ida  correcta 
	 * si no es correcta  pedira otra hasta que lo sea 
	 * si no es un numero lanza una exception 
	 * @param teclado  llamar teclado
	 * @param manejadorErrores maneja los posibles errores de fecha 
	 * @return devuelve una fecha valida*/
	public static int pedirFechaIda(Scanner teclado, ManejadorErrores manejadorErrores) {
		int fechaIda = 0;
		while (fechaIda < 1 || fechaIda > 30) {
			System.out.println("Fecha ida: ");
			try {
				fechaIda = teclado.nextInt();
			} catch (InputMismatchException error) {
				manejadorErrores.manejar(error);
				teclado.next();
			}
		}
		return fechaIda;
	}
	
	/**
	 * Metodo para pedir un origen valido hasta que lo sea
	 *@param buscadorVuelo llama al buscador vuelo
	 *@param teclado llama al teclado
	 *@param manejadorErrores maneja posibles errores de origen
	 * @return devuelve una origen valido */
	public static String pedirOrigen(BuscadorVuelo buscadorVuelo, Scanner teclado, ManejadorErrores manejadorErrores) {
		return pedirCiudad(buscadorVuelo, teclado, manejadorErrores, "Origen: ");
	}
	/**
	 * Metodo para pedir un destino valido hasta que lo sea
	 *@param buscadorVuelo llama al buscador vuelo
	 *@param teclado llama al teclado
	 *@param manejadorErrores maneja posibles errores de destino
	 *  @return devuelve una destino valido**/
	public static String pedirDestino(BuscadorVuelo buscadorVuelo, Scanner teclado, ManejadorErrores manejadorErrores) {
		return pedirCiudad(buscadorVuelo, teclado, manejadorErrores, "Destino: ");
	}
	/**
	 * Metodo para pedir una ciudad valida hasta que lo sea utilizando buscadorVuelo.existeCiudad
	 *@param buscadorVuelo llama al buscador vuelo
	 *@param teclado llama al teclado
	 *@param manejadorErrores maneja posibles errores de pedir ciudad
	 * **/
	private static String pedirCiudad(BuscadorVuelo buscadorVuelo, Scanner teclado, ManejadorErrores manejadorErrores,
			String mensaje) {
		System.out.print("ciudad  : ");
		String origen = teclado.next();

		while (!buscadorVuelo.existeCiudad(origen)) {
			System.out.print("ciudad : ");
			origen = teclado.next();
		}

		return origen;
	}
	/**
	 * Metodo para pedir un vuelo existente por su codigo hasta que se ingresa uno valido
	 * @param vuelos es un mapa de vuelos
	 * @param teclado llama a teclado
	 * @param manejadorErrores maneja los posibles errores de pedir vuelo
	 * @return revuelve un vuelo valido**/
	public static Vuelo pedirVuelo(HashMap<Integer, Vuelo> vuelos, Scanner teclado, ManejadorErrores manejadorErrores) {
		Vuelo vuelo = null;

		while (vuelo == null) {
			try {
				System.out.println("Escribe codigo Vuelo");
				int codigoVuelo = teclado.nextInt();
				vuelo = vuelos.get(codigoVuelo);

				if (vuelo == null) {
					throw new VueloNoEncontradoException("Codigo de vuelo no valido");
				}
			} catch (Exception error) {
				manejadorErrores.manejar(error);
			}
		}

		return vuelo;
	}
	/**
	 * Este metodo pide un asiento hasta que se le ingrese uno valido 
	 * @param teclado llama a teclado
	 * @param vuelo llama un vuelo
	 * @return devuelve un asiento valido*/
	public static Asiento pedirAsiento(Scanner teclado, Vuelo vuelo) {
		Asiento asiento = null;
		
		while (asiento == null) {
			System.out.println("Codigo Asiento");
			String codigoAsiento = teclado.next();
			asiento = vuelo.getAvion().getAsientos().get(codigoAsiento);
		}
	
		return asiento;
	}

}
