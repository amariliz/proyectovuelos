import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 * * @author Amariliz
 * @version 4, 3 Junio 2018
 * 
 * */
public class Main {

	public static void main(String[] args) {

		Scanner teclado = new Scanner(System.in);

		// crear aerolinea
		Aerolinea ryanair = new Aerolinea("Ryanair");
		Aerolinea plusair = new Aerolinea("Plusair");
		Aerolinea maxair = new Aerolinea("Maxair");

		// crear avion
		// aviones mini
		Avion mini1 = new Avion(4, "r1", ryanair, true);
		Avion mini2 = new Avion(4, "r2", ryanair, true);
		Avion mini3 = new Avion(4, "r3", ryanair, true);
		Avion mini4 = new Avion(4, "r4", ryanair, true);
		Avion mini5 = new Avion(4, "r5", ryanair, true);

		Avion mini6 = new Avion(4, "p1", plusair, true);
		Avion mini7 = new Avion(4, "p2", plusair, true);
		Avion mini8 = new Avion(4, "p3", plusair, true);
		Avion mini9 = new Avion(4, "p4", plusair, true);
		Avion mini10 = new Avion(4, "p5", plusair, true);

		Avion mini11 = new Avion(4, "m1", maxair, true);
		Avion mini12 = new Avion(4, "m2", maxair, true);
		Avion mini13 = new Avion(4, "m3", maxair, true);
		Avion mini14 = new Avion(4, "m4", maxair, true);
		Avion mini15 = new Avion(4, "m5", maxair, true);

		// aviones grandes
		Avion maxi1 = new Avion(10, "R1", ryanair, false);
		Avion maxi2 = new Avion(10, "R2", ryanair, false);
		Avion maxi3 = new Avion(10, "R3", ryanair, false);
		Avion maxi4 = new Avion(10, "R4", ryanair, false);
		Avion maxi5 = new Avion(10, "R5", ryanair, false);
		Avion maxi6 = new Avion(10, "R6", ryanair, false);

		Avion maxi7 = new Avion(10, "P1", plusair, false);
		Avion maxi8 = new Avion(10, "P2", plusair, false);
		Avion maxi9 = new Avion(10, "P3", plusair, false);
		Avion maxi10 = new Avion(10, "P4", plusair, false);
		Avion maxi11 = new Avion(10, "P5", plusair, false);
		Avion maxi12 = new Avion(10, "P6", plusair, false);

		Avion maxi13 = new Avion(10, "M1", maxair, false);
		Avion maxi14 = new Avion(10, "M2", maxair, false);
		Avion maxi15 = new Avion(10, "M3", maxair, false);
		Avion maxi16 = new Avion(10, "M4", maxair, false);
		Avion maxi17 = new Avion(10, "M5", maxair, false);
		Avion maxi18 = new Avion(10, "M6", maxair, false);

		// creacion de vuelos

		BuscadorVuelo buscador = new BuscadorVuelo();/* OJO */
		Vuelo malagaMadrid2 = new Vuelo(1, 1, buscador.getCiudad("Malaga"), buscador.getCiudad("Madrid"), mini3);
		Vuelo malagaMadrid = new Vuelo(1, 1, buscador.getCiudad("Malaga"), buscador.getCiudad("Madrid"), mini1);
		Vuelo malagaBarcelona = new Vuelo(1, 1, buscador.getCiudad("Malaga"), buscador.getCiudad("Barcelona"), mini2);
		Vuelo malagaViena = new Vuelo(1, 1, buscador.getCiudad("Malaga"), buscador.getCiudad("Viena"), maxi1);
		Vuelo malagaLinz = new Vuelo(1, 1, buscador.getCiudad("Malaga"), buscador.getCiudad("Linz"), maxi2);
		Vuelo malagaGraz = new Vuelo(1, 1, buscador.getCiudad("Malaga"), buscador.getCiudad("Graz"), maxi3);
		Vuelo malagaParis = new Vuelo(1, 1, buscador.getCiudad("Malaga"), buscador.getCiudad("Paris"), maxi4);
		Vuelo malagaLyon = new Vuelo(1, 1, buscador.getCiudad("Malaga"), buscador.getCiudad("Lyon"), maxi5);
		Vuelo malagaNantes = new Vuelo(1, 1, buscador.getCiudad("Malaga"), buscador.getCiudad("Nantes"), maxi6);
		Vuelo malagaFrankfurt = new Vuelo(1, 1, buscador.getCiudad("Malaga"), buscador.getCiudad("Frankfurt"), maxi7);
		Vuelo malagaBerlin = new Vuelo(1, 1, buscador.getCiudad("Malaga"), buscador.getCiudad("Berlin"), maxi8);
		Vuelo malagaMunich = new Vuelo(1, 1, buscador.getCiudad("Malaga"), buscador.getCiudad("Munich"), maxi9);
		Vuelo malagaRoma = new Vuelo(1, 1, buscador.getCiudad("Malaga"), buscador.getCiudad("Roma"), maxi10);
		Vuelo malagaMilan = new Vuelo(1, 1, buscador.getCiudad("Malaga"), buscador.getCiudad("Milan"), maxi11);
		Vuelo malagaVenecia = new Vuelo(1, 1, buscador.getCiudad("Malaga"), buscador.getCiudad("Venecia"), maxi12);

		Vuelo[] guardarVuelos = { malagaMadrid, malagaBarcelona, malagaViena, malagaLinz, malagaGraz, malagaParis,
				malagaLyon, malagaNantes, malagaFrankfurt, malagaBerlin, malagaMunich, malagaRoma, malagaMilan,
				malagaVenecia, malagaMadrid2 };

		buscador.setVuelos(guardarVuelos);
		
		ManejadorErrores manejadorErrores = new ManejadorErrores();

		ArrayList<Vuelo> GuardarVuelos = new ArrayList<>();
		System.out.println(guardarVuelos.length);

		for (int i = 0; i < guardarVuelos.length; ++i) {
			GuardarVuelos.add(guardarVuelos[i]);
		}

		HashMap<Integer, Vuelo> vuelos = new HashMap<>();

		while (vuelos.isEmpty()) {
			int fechaIda = Utilidades.pedirFechaIda(teclado, manejadorErrores);
			String origen = Utilidades.pedirOrigen(buscador, teclado, manejadorErrores);
			String destino = Utilidades.pedirDestino(buscador, teclado, manejadorErrores);

			vuelos = buscador.getVuelosDisponibles(fechaIda, buscador.getCiudad(origen), buscador.getCiudad(destino));

			if (vuelos.isEmpty()) {
				manejadorErrores.manejar(new VueloNoEncontradoException("No hay ningun vuelo para su seleccion"));
			}
		}

		System.out.println(vuelos.toString());

		Vuelo vuelo = Utilidades.pedirVuelo(vuelos, teclado, manejadorErrores);

		Asiento asiento = Utilidades.pedirAsiento(teclado, vuelo);
		Conexion.conectar();
		//para iniciar base de datos por primera vez.
		//Conexion.iniciarBaseDeDatos();
	    PedirClientej cl=new PedirClientej();
	   
	 
	 
		Cliente cliente = new Cliente(FormularioCliente.numeroTarjeta);

		try {
				cliente.consultarBD();
				System.out.println("\nBienvenido\n");
		} catch (ClienteNoEncontradoException error) {
			manejadorErrores.manejar(error);
			cliente = new Cliente(FormularioCliente.nombre, FormularioCliente.apellido, FormularioCliente.dni,
					FormularioCliente.correo, FormularioCliente.numeroTarjeta);
			cliente.insertar();
		}

		Reserva reserva = new Reserva(cliente, vuelo, asiento);
		asiento.setOcupado(true);
		System.out.println(vuelo.getAvion().getAsientos().toString());
		
	System.out.println(reserva.toString());
	
	}
	

	}


