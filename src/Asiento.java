/**
 * En este metodo es para crear un Asiento 
 * @author Amariliz
 * 
 * */

public class Asiento {
	private String Coordenada;
	private boolean ocupado;
	
	//contructor
	public Asiento (String coordeana,boolean ocupado){
		this.Coordenada=coordeana;
		this.ocupado=ocupado;
	}
	
	// getters and setters
	public String getCoordenada() {
		return Coordenada;
	}

	public void setCoordenada(String coordenada) {
		Coordenada = coordenada;
	}
	/**
	 * metodo para saber si el asiento esta ocupado 
	 * @return retorna si esta ocupado
	 * */
	public boolean isOcupado() {
		return ocupado;
	}
	/**
	 * metodo para setear asiento  a ocupado
	 * @param ocupado para setter el asiento a ocupado
	 * */
	public void setOcupado(boolean ocupado) {
		
		this.ocupado = ocupado;
	}


	
	@Override
	public String toString() {
		return " \n Coordenada: " + Coordenada + " Ocupado: " + ocupado +"\n";
	}

}
