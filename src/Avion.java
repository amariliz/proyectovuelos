
/**
 * Clase para crear un avion que se usara en Vuelo 
 * contiene a Aerolinea 
 * @author Amariliz
 * 
 * */
import java.util.HashMap;

public class Avion {
	private int asientosDisponibles;
	private String codigoAvion;
	private Aerolinea aerolinea;
	private HashMap<String,Asiento> asientos;
	private boolean esMini;
	
	public Avion(int asientosDisponibles,String cAvion,Aerolinea aerolinea,boolean esMini){
		this.asientosDisponibles=asientosDisponibles ;
		this.codigoAvion=cAvion;
		this.aerolinea=aerolinea;
		this.esMini=esMini;
		this.setAsientos(esMini);
		
	}
	
	
	public int getAsientosDisponibles() {
		return asientosDisponibles;
	}
	public void setAsientosDisponibles(int asientosDisponibles) {
		this.asientosDisponibles = asientosDisponibles;
	}
	public String getCodigoAvion() {
		return codigoAvion;
	}
	public void setCodigoAvion(String codigoAvion) {
		this.codigoAvion = codigoAvion;
	}
	public Aerolinea getAerolinea() {
		return aerolinea;
	}
	public void setAerolinea(Aerolinea aerolinea) {
		this.aerolinea = aerolinea;
	}
	
	/**
	 * metodo para crear Asientos 
	 * si la variable boolean en el contructor de Avion es TRUE creara 4 Asientos 
	 * ya definidos  sino creara 10, seteados por defecto a false de ocupado.
	 * 
	 * @return retorna un mapa de los asientos
	 * */
	public HashMap<String, Asiento> getAsientos() {
		return asientos;
	}
	private void setAsientos(boolean esMini) {
		asientos=new HashMap<String,Asiento>();
		if(esMini){
			//aviones pequeños total 4
			asientos.put("1a", new Asiento("1a",false));
			asientos.put("1b", new Asiento("1b",false));
			asientos.put("2a", new Asiento("2a",false));
			asientos.put("2b", new Asiento("2b",false));
		}else{
			//aviones grandes total 10
			asientos.put("1a", new Asiento("1a",false));
			asientos.put("1b", new Asiento("1b",false));
			asientos.put("2a", new Asiento("2a",false));
			asientos.put("2b", new Asiento("2b",false));
			asientos.put("3a", new Asiento("3a",false));
			asientos.put("3b", new Asiento("3b",false));
			asientos.put("4a", new Asiento("4a",false));
			asientos.put("4b", new Asiento("4b",false));
			asientos.put("5a", new Asiento("5a",false));
			asientos.put("5b", new Asiento("5b",false));
		}
	}
	public boolean isEsMini() {
		return esMini;
	}
	public void setEsMini(boolean esMini) {
		this.esMini = esMini;
	}
	
	/**
	 * Metodo para coger los Asientos Libres 
	 * te devuelve un mapa con los asientos disponibles
	 * @return el metodo retornara un hashmap con lo asientos que no esten ocupados 
	 * */
	public HashMap<String,Asiento> asientosLibres(){
		HashMap<String,Asiento>asientosLibres=new HashMap<>();
		for (Asiento asiento : this.asientos.values()) {
		    if(!asiento.isOcupado()){
		    	asientosLibres.put(asiento.getCoordenada(),asiento);
		    }
		}
		return asientosLibres;
	}
	/**resta un numero de asiento**/
	public void restarAsiento(){
		--this.asientosDisponibles;
	}
	@Override
	public String toString() {
		return "--Avión-- \n código Avion: " + codigoAvion + "\n Aerolinea: "
				+ aerolinea+" \n Asientos Disponibles: " + asientosDisponibles +  " \n Posiciones: " + asientosLibres()+"---" ;
	}
	



}
