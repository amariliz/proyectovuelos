
/**
 * Clase para Crear paises , se usa en ciudades 
 * */
public class Pais {
	private String nombre ;

	//contructor
	Pais(String nombre){
		this.nombre=nombre;
	}
	
	//setters and getters
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return "Pais [nombre=" + nombre + "]";
	}
	
	
	
	
}
