

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Clase para buscar Vuelos 
 * Se crean los paises y ciudades y se añaden a una lista teniendo asi
 * todas las ciudades en las que se puede volar
 * @author Amariliz
 * */
public class BuscadorVuelo {
	private HashMap<String, Ciudad> ciudades;
	private Vuelo[] vuelos;
	private ArrayList<Vuelo> Vuelos2;


	public BuscadorVuelo() {

		Pais espana = new Pais("España");
		Pais austria = new Pais("Austria");
		Pais francia = new Pais("Francia");
		Pais alemania = new Pais("Alemania");
		Pais italia = new Pais("Italia");

		ciudades = new HashMap<String, Ciudad>();
		ciudades.put("Malaga", new Ciudad("Malaga", espana));
		ciudades.put("Madrid", new Ciudad("Madrid", espana));
		ciudades.put("Barcelona", new Ciudad("Barcelona", espana));

		ciudades.put("Viena", new Ciudad("Viena", austria));
		ciudades.put("Linz", new Ciudad("Linz", austria));
		ciudades.put("Graz", new Ciudad("Graz", austria));
		ciudades.put("paris", new Ciudad("Paris", francia));
		ciudades.put("Lyon", new Ciudad("lyon", francia));
		ciudades.put("Nantes", new Ciudad("Nantes", francia));
		ciudades.put("Frankfurt", new Ciudad("Frankfurt", alemania));
		ciudades.put("Berlin", new Ciudad("Berlin", alemania));
		ciudades.put("Munich", new Ciudad("Munich", alemania));
		ciudades.put("Roma", new Ciudad("Roma", italia));
		ciudades.put("Milan", new Ciudad("Milan", italia));
		ciudades.put("venecia", new Ciudad("Venecia", italia));
		
	}

	/**
	 * Metodo para  buscar una ciudad  en el hashmap 
	 * si existe devuelve true sino false 
	 * 
	 * @param bus almacena una ciudad
	 * @return  retorna si esa ciudad existe
	 **/
	public boolean existeCiudad(String bus) {
		if (ciudades.get(bus) == null) {
			return false;
		}
		return true;
	}

	public Ciudad getCiudad(String bus) {
		return ciudades.get(bus);
	}

	public Vuelo[] getVuelos() {
		return vuelos;
	}

	public void setVuelos(Vuelo[] vuelos) {
		this.vuelos = vuelos;
	}

	public ArrayList<Vuelo> getVuelos2() {
		return Vuelos2;
	}

	public void setVuelos2(ArrayList<Vuelo> vuelos2) {
		Vuelos2 = vuelos2;
	}
	
	
/**
 * Metodo para devolver una lista de los vuelos disponibles segun los parametros 
 * que se le han pedido al usuario
 * @param fecha fecha elegida para el vuelo
 * @param origen ciudad elegida para el vuelo
 * @param destino destino elegido para el vuelo
 * @return devuelve mapa de vuelos
 * */
	public HashMap<Integer,Vuelo> getVuelosDisponibles(int fecha, Ciudad origen, Ciudad destino) {

		HashMap<Integer,Vuelo> vuelos = new HashMap<>();

		for (int i = 0; i < this.vuelos.length; ++i) {
			int fch = this.vuelos[i].getFechaSalida();

			if (fch == fecha && this.vuelos[i].getOrigen() == origen && this.vuelos[i].getDestino() == destino) {
				System.out.println("Insertando vuelo");
				vuelos.put(this.vuelos[i].getCodigo(), this.vuelos[i]);
			}

		}
		return vuelos;
	}
	

	@Override
	public String toString() {
		return "BuscadorVuelo [vuelos=" + Arrays.toString(vuelos) + "]";
	}

	

}
