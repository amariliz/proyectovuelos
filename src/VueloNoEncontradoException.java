
/**
 * Excepcion ,se usa cuando no se encuentra el Vuelo
 * */
public class VueloNoEncontradoException extends Exception{
	private static final long serialVersionUID = 1L;
	
	
	public VueloNoEncontradoException(String mensaje) {
		super(mensaje);
	}
	
}
