import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Dialog.ModalityType;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.FormSpecs;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import javax.swing.Action;
import java.awt.event.ActionListener;

public class PedirClienteNuevoj extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField jsNombreNuevoCliente;
	private JTextField jsApellidoNuevoCliente;
	private JTextField jsDniNuevoCliente;
	private JTextField jsCorreoNuevoCliente;
	private JTextField jsTarjetaNuevoCliente;
	private final Action action = new SwingAction();
	private final Action action_1 = new SwingAction_1();

	/**
	 * Launch the application.
	 * @param args main pedir cliente nuevo
	 */
	public static void main(String[] args) {
		try {
			PedirClienteNuevoj dialog = new PedirClienteNuevoj();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public PedirClienteNuevoj() {
		setModalityType(ModalityType.APPLICATION_MODAL);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JLabel lblNombre = new JLabel("Nombre");
			lblNombre.setBounds(85, 45, 50, 16);
			contentPanel.add(lblNombre);
		}
		{
			jsNombreNuevoCliente = new JTextField();
			jsNombreNuevoCliente.setBounds(139, 40, 306, 26);
			contentPanel.add(jsNombreNuevoCliente);
			jsNombreNuevoCliente.setColumns(10);
		}
		{
			JLabel lblApellido = new JLabel("Apellido");
			lblApellido.setBounds(83, 76, 52, 16);
			contentPanel.add(lblApellido);
		}
		{
			jsApellidoNuevoCliente = new JTextField();
			jsApellidoNuevoCliente.setBounds(139, 71, 306, 26);
			contentPanel.add(jsApellidoNuevoCliente);
			jsApellidoNuevoCliente.setColumns(10);
		}
		{
			JLabel lblNewLabel = new JLabel("Dni");
			lblNewLabel.setBounds(113, 107, 22, 16);
			contentPanel.add(lblNewLabel);
		}
		{
			jsDniNuevoCliente = new JTextField();
			jsDniNuevoCliente.setBounds(139, 102, 306, 26);
			contentPanel.add(jsDniNuevoCliente);
			jsDniNuevoCliente.setColumns(10);
		}
		{
			JLabel lblCorreo = new JLabel("Correo");
			lblCorreo.setBounds(93, 138, 42, 16);
			contentPanel.add(lblCorreo);
		}
		{
			jsCorreoNuevoCliente = new JTextField();
			jsCorreoNuevoCliente.setBounds(139, 133, 306, 26);
			contentPanel.add(jsCorreoNuevoCliente);
			jsCorreoNuevoCliente.setColumns(10);
		}
		{
			JLabel lblNumeroTarjeta = new JLabel("Numero Tarjeta");
			lblNumeroTarjeta.setBounds(38, 169, 97, 16);
			contentPanel.add(lblNumeroTarjeta);
		}
		{
			jsTarjetaNuevoCliente = new JTextField();
			jsTarjetaNuevoCliente.setBounds(139, 164, 306, 26);
			contentPanel.add(jsTarjetaNuevoCliente);
			jsTarjetaNuevoCliente.setColumns(10);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setAction(action);
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setAction(action_1);
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		setVisible(true);
	}

	private class SwingAction extends AbstractAction {
		public SwingAction() {
			putValue(NAME, "Aceptar");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}
		/**
		 * Boton Aceptar que al pulsar Guarda los datos que se han ingresado en la ventana a FormularioCliente 
		 * @param e se le pasa la accion del boton aceptar*/
		public void actionPerformed(ActionEvent e) {
			FormularioCliente.nombre = jsNombreNuevoCliente.getText();
			FormularioCliente.apellido = jsApellidoNuevoCliente.getText();
			FormularioCliente.dni = jsDniNuevoCliente.getText();
			FormularioCliente.correo = jsCorreoNuevoCliente.getText();
			FormularioCliente.numeroTarjeta = Integer.parseInt(jsTarjetaNuevoCliente.getText());
			dispose();
		}
	}
	/**boton cancelar*
	*/
	private class SwingAction_1 extends AbstractAction {
		public SwingAction_1() {
			putValue(NAME, "Cancelar");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}
		public void actionPerformed(ActionEvent e) {
			dispose();
		}
	}
	
}
