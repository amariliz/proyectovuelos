/**
	 * Exception, se usa cuando no se encuentra el Cliente 
	 * @author Amariliz
	 */

public class ClienteNoEncontradoException extends Exception{

	
	private static final long serialVersionUID = 1L;
	
	public ClienteNoEncontradoException() {
		super();
	}
	
	public ClienteNoEncontradoException(String mensaje) {
		super(mensaje);
	}
	

}
