/**
 * Clase para Crear ciudades usando la Clase Pais 
 * @author Amariliz
 * 
 * */
public class Ciudad{


	private String nombre;
	private Pais p;
	
	public Ciudad(String nombre, Pais p){
		this.nombre=nombre;
		this.p=p;
	}
	
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return nombre ;
	}

	
	
	
}
