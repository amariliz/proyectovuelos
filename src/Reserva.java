
/**
 * esta clase es donde se guarda la Reserva  , utiliza Cliente Vuelo y Asiento 
 * esta clase es solo para mostrar los datos de la reserva al cliente al finalizar el programa.
 * */
public class Reserva {
	//atributos 
	private Cliente cliente;
	private Vuelo vuelo;
	private Asiento asiento;
	
	
	//contructor 
	public Reserva(Cliente cliente, Vuelo vuelo,Asiento asiento ){
		this.cliente=cliente;
		this.vuelo=vuelo;
		this.asiento=asiento;	
		System.out.println("haciendo reserva");
	}
	

	
	
	
	//setters and getters
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Vuelo getVuelo() {
		return vuelo;
	}

	public void setVuelo(Vuelo vuelo) {
		this.vuelo = vuelo;
	}

	public Asiento getAsiento() {
		return asiento;
	}

	public void setAsiento(Asiento asiento) {
		this.asiento = asiento;
	}



	@Override
	public String toString() {
		return "\n-----Datos Reserva----\n\n Cliente=" + cliente + "\n vuelo=" + vuelo + "\n asiento=" + asiento + "]";
	}



}
