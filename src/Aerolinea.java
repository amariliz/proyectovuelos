
/**
 * Aerolinea se usara en el contructor de Avion 
 * @author Amariliz
 * 
 * 
 * 
 * */


public class Aerolinea {
	private String nombre;

	
	//contructor
	public Aerolinea(String nombre) {
		this.nombre=nombre;
	}
	
	//getters and setters
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return  nombre ;
	}

	
}
